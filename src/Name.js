import PropTypes from 'prop-types';

export default function Name({ fist, last, parenthandleChanges }) {
  function Profile() {
    return <img src="https://i.imgur.com/QIrZWGIs.jpg" alt="Alan L. Hart" />;
  }

  return (
    <section>
      <h1>Amazing scientists name component</h1>
      <Profile />
      <div onClick={parenthandleChanges}>
        name: {fist} {last} dari depan
      </div>
    </section>
  );
}

Name.propTypes = {
  first: PropTypes.string,
  last: PropTypes.string
};
