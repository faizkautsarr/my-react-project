// import './App.css';
// import React from 'react';
// import C from './components/c';
// import { Link } from 'react-router-dom';

// class A extends React.Component {
//   render() {
//     return <h1>buttonA, {this.props.name}</h1>;
//   }
// }

// const B = (props) => {
//   return (
//     <h1 style={{ backgroundColor: `${props.color}` }}>buttonB {props.name}</h1>
//   );
// };

// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       count: 0
//     };
//   }
//   setClick = () => {
//     console.log(this.state.count);
//     this.setState = {
//       count: count + 1
//     };
//     // this.setState = {
//     //   count: count + 1
//     // };
//   };
//   render() {
//     return (
//       <div className="App" style={{ padding: 24 }}>
//         halo
//         <A></A>
//         <Link to="/page2"> link to page 2 </Link>
//         <B name="koa" color="pink"></B>
//         <C name="faiz" color="pink"></C>
//         <div onClick={this.setClick}> lol </div>
//       </div>
//     );
//   }
// }

import { useState } from 'react';
import Name from './Name';
import Inside from './Inside';
import Outside from './Outside';
import { Link } from 'react-router-dom';

export default function App() {
  const arr = [1, 2, 3, 4];
  const [index, setIndex] = useState(0);
  const [inputName, setName] = useState('');

  const people = [
    {
      id: 0,
      name: 'Creola Katherine Johnson',
      profession: 'mathematician',
      accomplishment: 'spaceflight calculations',
      imageId: 'MK3eW3A'
    },
    {
      id: 1,
      name: 'Mario José Molina-Pasquel Henríquez',
      profession: 'chemist',
      accomplishment: 'discovery of Arctic ozone hole',
      imageId: 'mynHUSa'
    },
    {
      id: 2,
      name: 'Mohammad Abdus Salam',
      profession: 'physicist',
      accomplishment: 'electromagnetism theory',
      imageId: 'bE7W1ji'
    },
    {
      id: 3,
      name: 'Percy Lavon Julian',
      profession: 'chemist',
      accomplishment:
        'pioneering cortisone drugs, steroids and birth control pills',
      imageId: 'IOjWm71'
    },
    {
      id: 4,
      name: 'Subrahmanyan Chandrasekhar',
      profession: 'astrophysicist',
      accomplishment: 'white dwarf star mass calculations',
      imageId: 'lrWQx8l'
    }
  ];

  function addCounter() {
    setIndex(index + 1);
    console.log(index);
  }

  // function ClearName() {
  //   if (index % 2 === 0) {
  //     return <li className="item"> faiz kautsar xx </li>;
  //   }
  //   return <li className="item">faiz kautsar yy</li>;
  // }

  function subtractCounter() {
    setIndex(index - 1);
    console.log(index);
    console.log(arr);
    console.log(arr.filter((a) => a > 2));
    console.log(arr);
  }

  return (
    <>
      <img src="https://i.imgur.com/lICfvbD.jpg" alt="Aklilu Lemma" />
      <button onClick={addCounter}>Plus</button>
      <button onClick={subtractCounter}>Minus</button>
      <div> {index} </div>
      <div> {arr} </div>
      <Link to="/page2"> link to page 2 </Link>

      <Name
        fist={inputName}
        last={'Hart'}
        parenthandleChanges={() => console.log('halo from child')}
      ></Name>
      <Outside>
        <Inside> </Inside>
      </Outside>

      <textarea
        onchange
        value={inputName}
        onChange={(e) => setName(e.target.value)}
        disabled={inputName.includes('faiz')}
      />
      <p> {inputName} </p>
      {index % 2 === 0 ? (
        <li className="item"> faiz kautsar xx </li>
      ) : (
        <li className="item">faiz kautsar yy</li>
      )}
      {people.map((p) => (
        <li key={p.id}>
          <p>
            <b>{p.name}:</b>
            {' ' + p.profession + ' '}
            known for {p.accomplishment}
          </p>
        </li>
      ))}
    </>
  );
}
