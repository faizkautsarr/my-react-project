import './App.css';
import React from 'react';
import C from './components/c';

function App() {
  return (
    <div className="App" style={{ padding: 24 }}>
      halo page 2<C name="faiz" color="pink"></C>
    </div>
  );
}

export default App;
